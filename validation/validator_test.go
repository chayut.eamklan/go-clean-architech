package validation_test

import (
	"domain/model"
	"domain/validation"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidator(t *testing.T) {
	validation.Init()
	req_test := `{
		"name": ""
	}`
	req := model.Request{}
	json.Unmarshal([]byte(req_test), &req)
	err := validation.Validate(req)
	assert.Error(t,err)

	validation.Init()
	req_test = `{
		"name": "564565"
	}`
	req = model.Request{}
	json.Unmarshal([]byte(req_test), &req)
	err = validation.Validate(req)
	assert.NoError(t,err)

	unbufferedChannel := make(chan int)
	err = validation.Validate(unbufferedChannel)
	assert.Error(t,err)
}
