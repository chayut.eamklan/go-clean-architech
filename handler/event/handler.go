package event

import (
	"encoding/json"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"

	"domain/model"
	"domain/service"
	v "domain/validation"

	"github.com/sirupsen/logrus"
)

// interface
type HelloHandler interface {
	HelloHandler(c *fiber.Ctx) error
}

// concrete implementation
type HelloHandlerImpl struct {
	logger  *logrus.Entry
	service *service.Service
}

// constructor
func NewHelloHandlerImpl(service *service.Service, logger *logrus.Entry) *HelloHandlerImpl {
	return &HelloHandlerImpl{
		logger:  logger,
		service: service,
	}
}

func (h *HelloHandlerImpl) HelloHandler(c *fiber.Ctx) error {
	req := model.Request{}
	if err := json.Unmarshal(c.Body(), &req); err != nil {
		return err
	}

	logs := h.logger.WithFields(logrus.Fields{
		"route":    c.Route().Path,
		"trace id": c.Get("X-Trace-Id", uuid.New().String()),
	})

	// validate
	if err := v.Validate(&req); err != nil {
		return err
	}

	logs.Info("Start Hello")

	res := h.service.Hello(req, logs)

	return c.Status(200).JSON(res)
}
