package config

import (
	"strings"
	"time"

	er "domain/error"
	"domain/handler/event"
	"domain/handler/meta"
	"domain/model"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

type APIServer struct {
	server *fiber.App
	addr   string
	logger *logrus.Entry
}

func newAPIServer(fiberConfig model.FiberConfig, appInfo model.AppInfo, eventarcHandler event.HelloHandler, metaHandler meta.MetaHandler, errorResponseHandler *er.ErrorResponseHandler, logger *logrus.Entry) App {
	config := fiber.Config{
		ErrorHandler:          errorResponseHandler.ErrorHandler,
		ReadTimeout:           time.Millisecond * time.Duration(fiberConfig.ReadTimeout),
		WriteTimeout:          time.Millisecond * time.Duration(fiberConfig.WriteTimeout),
		IdleTimeout:           time.Millisecond * time.Duration(fiberConfig.IdleTimeout),
		AppName:               appInfo.Name,
		DisableStartupMessage: !appInfo.Banner,
		Immutable:             true,
	}

	// middleware

	fiber := fiber.New(config)
	fiber.Get("/info", metaHandler.Info())
	fiber.Get("/health", metaHandler.Health())
	fiber.Post("/hello", eventarcHandler.HelloHandler)

	app := &APIServer{
		server: fiber,
		addr:   fiberConfig.Address,
		logger: logger,
	}

	return app
}

func (this *APIServer) Start() {
	this.logger.Infof("Starting API Server on port : %v ...", strings.Split(this.addr, ":")[1])
	if err := this.server.Listen(this.addr); err != nil {
		this.logger.Fatalf("Error starting API Server. Caused by: %v.", err)
	}
}

func (this *APIServer) Stop() {
	this.logger.Info("Stopping API Server...")
	if err := this.server.Shutdown(); err != nil {
		this.logger.Errorf("Error stopping API Server. Caused by: %v.", err)
	}
}
