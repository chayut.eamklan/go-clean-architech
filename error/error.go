package error

type (
	BaseError struct {
		Code    string
		Message string
		Detail  interface{}
	}

	ServerError struct {
		BaseError
	}

	GCSReadError struct {
		BaseError
	}
)

func (e BaseError) Error() string {
	return e.Message
}

func NewServerError(errmsg string, detail interface{}) ServerError {
	return ServerError{
		BaseError{
			Code:    "500",
			Message: errmsg,
			Detail:  detail,
		},
	}
}

func NewFieldValidationError(errmsg string) ServerError {
	return ServerError{
		BaseError{
			Code:    "400",
			Message: errmsg,
			Detail:  nil,
		},
	}
}
