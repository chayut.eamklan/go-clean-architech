package error

import (
	"encoding/json"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

type ErrorResponseHandler struct {
	logger *logrus.Entry
}

func NewErrorResponseHandler(logger *logrus.Entry) *ErrorResponseHandler {
	return &ErrorResponseHandler{
		logger: logger,
	}
}

func (h *ErrorResponseHandler) ErrorHandler(c *fiber.Ctx, err error) error {
	// log error
	status, _ := h.getStatusAndDetail(err)
	errResponse := NewErrorResponse(err)
	payload, _ := jsonMarshal(errResponse)

	h.logger.WithFields(logrus.Fields{
			"status": status, 
			"payload": payload}).Error(err.Error())

	// return error response
	return c.Status(status).JSON(errResponse)
}

func (h *ErrorResponseHandler) getStatusAndDetail(err error) (int, interface{}) {
	switch err.(type) {
	case ServerError:
		e, _ := err.(ServerError)
		code, _ := strconv.Atoi(e.Code)
		return code, e.Detail
	case *fiber.Error:
		e, _ := err.(*fiber.Error)
		return e.Code, e
	default:
		return 500, nil
	}
}

func jsonMarshal(i interface{}) (string, error) {
	b, err := json.Marshal(i)
	if err != nil {
		return "", err
	}
	return string(b), nil
}
