package service

import (
	"domain/model"
	"domain/repository"

	"github.com/sirupsen/logrus"
)

type Service struct {
	repository repository.Repository
}

func NewService(repository repository.Repository) *Service {
	return &Service{
		repository: repository,
	}
}

func (s *Service) Hello(req model.Request, logger *logrus.Entry) model.Response {

	code, message := s.repository.Hello(req)
	logger.Infof("Success is code : %v and Message : %v", code, message)
	return model.NewResponse(code, message)
}
